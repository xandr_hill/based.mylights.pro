<?php


$muxb_page = 'muxb-options.php'; // part of URL of page

// function which add options page to admin panel

function muxb_options() {
	global $muxb_page;
	add_options_page( 'Theme options', 'MUXB Options', 'manage_options', $muxb_page, 'muxb_option_page');  
}
add_action('admin_menu', 'muxb_options');
 
// Callback function

function muxb_option_page(){
	global $muxb_page;
	?><div class="wrap">
		<h2>MUXB theme options page</h2>
		<form method="post" enctype="multipart/form-data" action="options.php">
			<?php 
			settings_fields('muxb_options'); // options name
			do_settings_sections($muxb_page);
			?>
			<p class="submit">  
				<input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />  
			</p>
		</form>
	</div><?php
}
 
function muxb_option_settings() {
	global $muxb_page;
	// Connecting validation function - muxb_validate_settings() 
	register_setting( 'muxb_options', 'muxb_options', 'muxb_validate_settings' ); // muxb_options

	add_settings_section( 'muxb_section_1', 'Page view options', '', $muxb_page );
 
	// Checkbox option
	$muxb_field_params = array(
		'type'      => 'checkbox',
		'id'        => 'my_checkbox',
		'desc'      => 'Apply to invert color-scheme.'
	);
	add_settings_field( 'my_checkbox_field', 'Invert colors', 'muxb_option_display_settings', $muxb_page, 'muxb_section_1', $muxb_field_params );
 

	// Adding list of supported languages
	$muxb_field_params = array(
		'type'      => 'select',
		'id'        => 'my_select',
		'desc'      => 'Choose the language.',
		'vals'		=> array( 'val1' => 'English', 'val2' => 'Espana', 'val3' => 'Русский', 'val4' => 'Hebrew')
	);
	add_settings_field( 'my_select_field', 'MUXB supported languages', 'muxb_option_display_settings', $muxb_page, 'muxb_section_1', $muxb_field_params );
 
	
	// Second section of options
 
	add_settings_section( 'muxb_section_2', 'Another options', '', $muxb_page );

	$muxb_field_params = array(
		'type'	=> 'radio',
		'id'	=> 'my_radio',
		'vals'	=> array( 'opt1' => 'Option 1', 'opt2' => 'Option 2', 'opt3' => 'Option 3', 'opt4' => 'Option 4' )
	);
	add_settings_field( 'my_radio_field', 'Some more options', 'muxb_option_display_settings', $muxb_page, 'muxb_section_2', $muxb_field_params );

}
 
add_action( 'admin_init', 'muxb_option_settings' );
 
/*
 * Функция отображения полей ввода
 * Здесь задаётся HTML и PHP, выводящий поля
 */
function muxb_option_display_settings($args) {
	extract( $args );
 
	$option_name = 'muxb_options';
 
	$o = get_option( $option_name );
 
	switch ( $type ) {  
		case 'text':  
			$o[$id] = esc_attr( stripslashes($o[$id]) );
			echo "<input class='regular-text' type='text' id='$id' name='" . $option_name . "[$id]' value='$o[$id]' />";  
			echo ($desc != '') ? "<br /><span class='description'>$desc</span>" : "";  
		break;
		case 'textarea':  
			$o[$id] = esc_attr( stripslashes($o[$id]) );
			echo "<textarea class='code large-text' cols='50' rows='10' type='text' id='$id' name='" . $option_name . "[$id]'>$o[$id]</textarea>";  
			echo ($desc != '') ? "<br /><span class='description'>$desc</span>" : "";  
		break;
		case 'checkbox':
			$checked = ($o[$id] == 'on') ? " checked='checked'" :  '';  
			echo "<label><input type='checkbox' id='$id' name='" . $option_name . "[$id]' $checked /> ";  
			echo ($desc != '') ? $desc : "";
			echo "</label>";  
		break;
		case 'select':
			echo "<select id='$id' name='" . $option_name . "[$id]'>";
			foreach($vals as $v=>$l){
				$selected = ($o[$id] == $v) ? "selected='selected'" : '';  
				echo "<option value='$v' $selected>$l</option>";
			}
			echo ($desc != '') ? $desc : "";
			echo "</select>";  
		break;
		case 'radio':
			echo "<fieldset>";
			foreach($vals as $v=>$l){
				$checked = ($o[$id] == $v) ? "checked='checked'" : '';  
				echo "<label><input type='radio' name='" . $option_name . "[$id]' value='$v' $checked />$l</label><br />";
			}
			echo "</fieldset>";  
		break; 
	}
}
 
/*
 * Функция проверки правильности вводимых полей
 */
function muxb_validate_settings($input) {
	foreach($input as $k => $v) {
		$valid_input[$k] = trim($v);
 
		/* Можно включить в эту функцию различные проверки значений, например
		if(! задаем условие ) { // если не выполняется
			$valid_input[$k] = ''; // тогда присваиваем значению пустую строку
		}
		*/
	}
	return $valid_input;
}

?>